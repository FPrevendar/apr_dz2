## 2\. domaća zadaća iz analize i projektiranja računalom

U ovoj domaćoj zadaći je bilo potrebno ostvarititi nekoliko metoda neograničenen optimizacije u jednoj ili više dimenzija, to su:
* Metoda zlatnog reza
* Pretraživanje po koordinatnim osima
* Simpleks postupak po Nelderu i Meadu 
* Hooke-Jeeves postupak

To je ostvareno u programskom jeziku java, i koristi se biblioteka za rad s matricama ostvarena u [1\.domaćoj zadaći](https://gitlab.com/FPrevendar/apr_dz1)

Tekst zadatka se nalazi [ovdje](https://www.fer.unizg.hr/_download/repository/vjezba_2%5B4%5D.pdf).
