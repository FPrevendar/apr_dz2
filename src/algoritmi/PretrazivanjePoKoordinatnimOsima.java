package algoritmi;

import funkcije.IFunction;
import point.IPoint;
import point.Point;

public class PretrazivanjePoKoordinatnimOsima {

	public static IPoint PretraziPoKoordinatnimOsima(IFunction funkcijaCilja, IPoint pocetnaTocka, IPoint preciznost){
		IPoint tockaX = pocetnaTocka.copy();
		IPoint xs;
		do {
			xs = tockaX.copy();
			for(int i = 0; i < tockaX.getDimenstion(); i++) {
				Interval interval = ZlatniRez.PostupakZlatnogReza(funkcijaCilja, tockaX, i, preciznost.get(i), 1);
				tockaX.set(i, (interval.left + interval.right)/2);
			}
		}while(tockaX.sub(xs).length() > 10e-6);
		return tockaX;
	}
	
	public static IPoint PretraziPoKoordinatnimOsima(IFunction funkcijaCilja, IPoint pocetnaTocka) {
		IPoint preciznost = Point.i(pocetnaTocka.getDimenstion());
		preciznost = preciznost.mul(10e-6);
		return PretraziPoKoordinatnimOsima(funkcijaCilja, pocetnaTocka, preciznost);
	}
}
