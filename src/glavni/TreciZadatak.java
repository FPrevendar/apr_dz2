package glavni;

import algoritmi.HookeJeeves;
import algoritmi.SimpleksNelderMead;
import funkcije.IFunction;
import funkcije.JakobovicevaFunkcija;
import point.IPoint;
import point.Point;

public class TreciZadatak {

	public static void main(String[] args) {
		System.out.println("Treci zadatak");
		IPoint pocetnaTocka = Point.ParsePoint("5 5");
		IFunction f4Simp = new JakobovicevaFunkcija();
		IFunction f4HJ = new JakobovicevaFunkcija();
		IPoint minimumSimp = SimpleksNelderMead.SimplexPostupakPoNelderMeadu(f4Simp, pocetnaTocka);
		IPoint minimumHJ = HookeJeeves.HookeJeevesPostupak(f4HJ, pocetnaTocka);
		
		System.out.println("Postupak Hooke Jeeves:");
		System.out.print("Pronadjeni minimum u tocki: ");
		minimumHJ.print();
		System.out.println("Vrijednost u tocki minimuma: " + f4HJ.getValueAtPoint(minimumHJ));
		System.out.println("Broj ispitivanja vrijednosti: " + f4HJ.getNumberOfCalls());
		System.out.println();
		System.out.println("Simpleks po Nelkderu i Meadu:");
		System.out.print("Pronadjeni minimum u tocki: ");
		minimumSimp.print();
		System.out.println("Vrijednost u tocki minimuma: " + f4Simp.getValueAtPoint(minimumSimp));
		System.out.println("Broj ispitivanja vrijednosti: " + f4Simp.getNumberOfCalls());
		System.out.println();
	}

}
