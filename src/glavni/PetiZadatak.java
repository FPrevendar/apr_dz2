package glavni;

import java.util.Random;

import algoritmi.SimpleksNelderMead;
import funkcije.IFunction;
import funkcije.SchafferovaFunkcijaF6;
import point.IPoint;
import point.Point;

public class PetiZadatak {

	public static void main(String[] args) {
		System.out.println("Peti Zadatak");
		
		IFunction fja = new SchafferovaFunkcijaF6(2);
		Random rand = new Random();
		int ukupnoPokusaja = 1000;
		int uspjesnihPronalazaka = 0;
		
		for(int i = 0; i < ukupnoPokusaja; i++) {
			IPoint pocetna = new Point(2);
			pocetna.set(0, rand.nextDouble() * 100 - 50);
			pocetna.set(1, rand.nextDouble() * 100 - 50);
			IPoint optimum = SimpleksNelderMead.SimplexPostupakPoNelderMeadu(fja, pocetna);
			if(fja.getValueAtPoint(optimum) < 10e-4) {
				System.out.print("Pronadjen optimum: ");
				optimum.print();
				System.out.println("Vrijednost u optimumu: " + fja.getValueAtPoint(optimum));
				uspjesnihPronalazaka++;
			}
		}
		System.out.println("Koristeno je " + ukupnoPokusaja + " pocetnih tocaka.");
		System.out.println("Samo je " + uspjesnihPronalazaka + " pronaslo optimum manji od 10e-4");
		System.out.println("Tu su drugacije vrijedosti za svako pokretanje jer su tocke random");
	}

}
