package glavni;

import algoritmi.HookeJeeves;
import algoritmi.PretrazivanjePoKoordinatnimOsima;
import algoritmi.SimpleksNelderMead;
import funkcije.FunkcijaBroj2;
import funkcije.FunkcijaBroj3;
import funkcije.IFunction;
import funkcije.JakobovicevaFunkcija;
import funkcije.RosenbrockovaBananaFunkcija;
import point.IPoint;
import point.Point;

public class DrugiZadatak {

	public static void main(String[] args) {
		
		System.out.println("Drugi zadatak");
		
		IFunction[] funkcije = new IFunction[4];
		funkcije[0] = new RosenbrockovaBananaFunkcija();
		funkcije[1] = new FunkcijaBroj2();
		funkcije[2] = new FunkcijaBroj3(5);
		funkcije[3] = new JakobovicevaFunkcija();
		
		IPoint[] pocetneTocke = new IPoint[4];
		pocetneTocke[0] = Point.ParsePoint("-1.9 2");
		pocetneTocke[1] = Point.ParsePoint("0.1 0.3");
		pocetneTocke[2] = Point.ParsePoint("0 0 0 0 0");
		pocetneTocke[3] = Point.ParsePoint("5.1 1.1");
		
		IPoint[][] tockeMinimuma = new IPoint[3][4];
		double[][] minimumi = new double[3][4];
		int[][] brojPoziva = new int[3][4];
		
		for(int i = 0; i < 4; i++) {
			tockeMinimuma[0][i] = PretrazivanjePoKoordinatnimOsima.PretraziPoKoordinatnimOsima(
					funkcije[i], pocetneTocke[i]);
			minimumi[0][i] = funkcije[i].getValueAtPoint(tockeMinimuma[0][i]);
			brojPoziva[0][i] = funkcije[i].getNumberOfCalls();
		}
		
		funkcije[0] = new RosenbrockovaBananaFunkcija();
		funkcije[1] = new FunkcijaBroj2();
		funkcije[2] = new FunkcijaBroj3(5);
		funkcije[3] = new JakobovicevaFunkcija();
		
		for(int i = 0; i < 4; i++) {
			tockeMinimuma[1][i] = SimpleksNelderMead.SimplexPostupakPoNelderMeadu(
					funkcije[i], pocetneTocke[i]);
			minimumi[1][i] = funkcije[i].getValueAtPoint(tockeMinimuma[1][i]);
			brojPoziva[1][i] = funkcije[i].getNumberOfCalls();
		}
		
		funkcije[0] = new RosenbrockovaBananaFunkcija();
		funkcije[1] = new FunkcijaBroj2();
		funkcije[2] = new FunkcijaBroj3(5);
		funkcije[3] = new JakobovicevaFunkcija();
		
		for(int i = 0; i < 4; i++) {
			tockeMinimuma[2][i] = HookeJeeves.HookeJeevesPostupak(
					funkcije[i], pocetneTocke[i]);
			minimumi[2][i] = funkcije[i].getValueAtPoint(tockeMinimuma[2][i]);
			brojPoziva[2][i] = funkcije[i].getNumberOfCalls();
		}
		
		String[] postupci = {"Koordinatne osi", "Nelder Mead Simpleks", "Hooke Jeeves"};
		String[] imenaFunkcija = {"Banana funkcija", "Funkcija 2", "Funkcija 3", "Jakoboviceva funkcija"};
		for(int j = 0; j < 4; j++) {
			System.out.println(imenaFunkcija[j]);
			for(int i = 0; i < 3; i++) {
				System.out.println(postupci[i] + " postupak. Minimum: " + minimumi[i][j]);
				System.out.print("U tocci: ");
				tockeMinimuma[i][j].print();
				System.out.println("Broj poziva: " + brojPoziva[i][j]);
			}
			System.out.println();
		}
		
	}

}
