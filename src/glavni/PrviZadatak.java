package glavni;

import algoritmi.HookeJeeves;
import algoritmi.Interval;
import algoritmi.PretrazivanjePoKoordinatnimOsima;
import algoritmi.SimpleksNelderMead;
import algoritmi.ZlatniRez;
import funkcije.FunkcijaBroj3;
import funkcije.IFunction;
import point.IPoint;
import point.Point;

public class PrviZadatak {

	public static void main(String[] args) {
		System.out.println("Prvi zadatak");
		for(int i = 10; i < 1500; i*= 5) {
			IFunction fja = new FunkcijaBroj3(1);
			IPoint pocetnaTocka = new Point(1);
			pocetnaTocka.set(0, i);
			Interval interval = ZlatniRez.postupakZlatnogReza(fja, pocetnaTocka);
			System.out.println("Zlatzni rez, pocetna tocka " + i);
			interval.tocka.set(interval.koordinata, (interval.left + interval.right)/2);
			System.out.print("Minimum je u tocki: ");
			interval.tocka.print();
			System.out.println("Vrijednost u tocki minimuma: " + fja.getValueAtPoint(interval.tocka));
			System.out.println("Broj ispitivanja: " + fja.getNumberOfCalls());
		}
		System.out.println();
		for(int i = 10; i < 1500; i*= 5) {
			IFunction fja = new FunkcijaBroj3(1);
			IPoint pocetnaTocka = new Point(1);
			pocetnaTocka.set(0, i);
			IPoint min = PretrazivanjePoKoordinatnimOsima.PretraziPoKoordinatnimOsima(fja, pocetnaTocka);
			System.out.println("Pretrazivanje po koordinatnim osima, pocetna tocka " + i);
			min.print();
			System.out.println("Vrijednost u tocki minimuma: " + fja.getValueAtPoint(min));
			System.out.println("Broj ispitivanja: " + fja.getNumberOfCalls());
		}
		System.out.println();
		for(int i = 10; i < 1500; i*= 5) {
			IFunction fja = new FunkcijaBroj3(1);
			IPoint pocetnaTocka = new Point(1);
			pocetnaTocka.set(0, i);
			IPoint min = HookeJeeves.HookeJeevesPostupak(fja, pocetnaTocka);
			System.out.println("Pretrazivanje Hooke Jeeves, pocetna tocka " + i);
			min.print();
			System.out.println("Vrijednost u tocki minimuma: " + fja.getValueAtPoint(min));
			System.out.println("Broj ispitivanja: " + fja.getNumberOfCalls());
		}
		System.out.println();
		for(int i = 10; i < 1500; i*= 5) {
			IFunction fja = new FunkcijaBroj3(1);
			IPoint pocetnaTocka = new Point(1);
			pocetnaTocka.set(0, i);
			IPoint min = SimpleksNelderMead.SimplexPostupakPoNelderMeadu(fja, pocetnaTocka);
			System.out.println("Pretrazivanje Nelder Mead, pocetna tocka " + i);
			min.print();
			System.out.println("Vrijednost u tocki minimuma: " + fja.getValueAtPoint(min));
			System.out.println("Broj ispitivanja: " + fja.getNumberOfCalls());
		}
	}

}
