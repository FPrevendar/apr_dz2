package glavni;

import algoritmi.SimpleksNelderMead;
import funkcije.IFunction;
import funkcije.RosenbrockovaBananaFunkcija;
import point.IPoint;
import point.Point;

public class CetvrtiZadatak {

	public static void main(String[] args) {
		System.out.println("Cetvrti zadatak");
		IFunction fja = new RosenbrockovaBananaFunkcija();
		IPoint pocetna05 = Point.ParsePoint("0.5 0.5");
		IPoint pocetna20 = Point.ParsePoint("20 20");
		
		System.out.println("Pocetna tocka (0.5, 0.5)");
		for(int i = 1; i < 20 + 1; i++) {
			fja = new RosenbrockovaBananaFunkcija();
			IPoint minimum = SimpleksNelderMead.SimplexPostupakPoNelderMeadu(fja, pocetna05, i, 1, 0.5, 2, 0.5, 10e-6);
			System.out.print("Korak = " + i + " Broj ispitivanja:" + fja.getNumberOfCalls() + " minimum: ");
			minimum.print();
		}
		
		System.out.println("Pocetna tocka (20, 20)");
		for(int i = 1; i < 20 + 1; i++) {
			fja = new RosenbrockovaBananaFunkcija();
			IPoint minimum = SimpleksNelderMead.SimplexPostupakPoNelderMeadu(fja, pocetna20, i, 1, 0.5, 2, 0.5, 10e-6);
			System.out.print("Korak = " + i + " Broj ispitivanja:" + fja.getNumberOfCalls() + " minimum: ");
			minimum.print();
		}
		System.out.println();
	}

}
