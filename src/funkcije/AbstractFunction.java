package funkcije;

import point.IPoint;

/*
 * Abstraktna klasa funkcije
 */
public abstract class AbstractFunction implements IFunction {

	private int numberOfCalls = 0;
	
	
	protected abstract double getValue(IPoint point);
	
	protected abstract boolean checkDimension(IPoint point);
	
	/* okvirna metoda */
	@Override
	public double getValueAtPoint(IPoint point) {
		if(checkDimension(point)) {
			numberOfCalls++;
			return getValue(point);
		}else {
			throw new IllegalArgumentException("Pogresna velicina ulaznog vektora");
		}
	}

	@Override
	public int getNumberOfCalls() {
		return numberOfCalls;
	}

}
