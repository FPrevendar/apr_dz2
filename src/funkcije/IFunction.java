package funkcije;

import point.IPoint;

/*
 * Sucelje koje predstavlja funkciju
 */
public interface IFunction {
	
	public double getValueAtPoint(IPoint point);
	
	public int getNumberOfCalls();
}
